# Define functions that can be re-used in GitLab CI jobs

export ARTIFACTORY_URL="https://artifactory.esss.lu.se/artifactory"
export VAGRANTCLOUD_API_URL="https://app.vagrantup.com/api/v1"

# !!!!DEPRECATED!!!
# Deploy artifacts to Artifactory
# Parameters:
#  $1: repository path
#  $2 $3 ...: artifacts to upload
#
# Example:
#  deploy_to_artifactory ics-conda/linux-64 pkg/linux-64/*.tar.bz2
function deploy_to_artifactory() {
  local target=$1
  shift
  local filepaths=$@

  echo "WARNING: this function is deprecated! Use the registry.esss.lu.se/ics-docker/jfrog docker image to interact with Artifactory"
  for filepath in $filepaths
  do
    filename=$(basename $filepath)
    curl -H "X-JFrog-Art-Api:${ARTIFACTORY_API_KEY}" -X PUT "${ARTIFACTORY_URL}/${target}/${filename}" -T "$filepath"
  done
}


# !!!!DEPRECATED!!!
# Launch a job template
# Parameters:
#  $1: job template
#
# Example:
#  launch_job_template deploy-csentry-staging
function launch_job_template() {
  local job_template=$1

  echo "WARNING: this function is deprecated! Use the registry.esss.lu.se/ics-docker/tower-cli docker image to interact with AWX"
  curl --user ${ANSIBLE_AWX_USERNAME}:${ANSIBLE_AWX_PASSWORD} --fail --header \'Content-Type: application/json\' --request POST ${ANSIBLE_AWX_API_URL}/job_templates/${job_template}/launch/
}


# !!!!DEPRECATED!!!
# Upload a Python source package and wheel to artifactory PyPI repository
# Parameters: None
#
# Example:
#  upload_to_artifactory_pypi
function upload_to_artifactory_pypi() {
  echo "WARNING: this function is deprecated! Use the ArtifactoryPyPI.gitlab-ci.yml template instead"
  cat <<EOF > ~/.pypirc
[distutils]
index-servers =
    artifactory

[artifactory]
repository: ${ARTIFACTORY_PYPI_URL}
username: ${ARTIFACTORY_PYPI_USER}
password: ${ARTIFACTORY_PYPI_PASSWORD}
EOF

  python setup.py check sdist bdist_wheel upload -r artifactory
  rm -f ~/.pypirc
}

# !!!!DEPRECATED!!!
# Upload a vagrant box to Vagrant Cloud
# Parameters:
#  $1: local box path
#  $2: box name
#  $3: box version
#
# The VAGRANTCLOUD_TOKEN variable shall be defined in the environment
#
# Example:
#  upload_to_vagrant_cloud build/esss-centos-7.box centos-7 1708.01
function upload_to_vagrant_cloud() {
  local box_path=$1
  local box_name=$2
  local box_version=$3
  local box_api_url=${VAGRANTCLOUD_API_URL}/box/esss/${box_name}

  echo "WARNING: this function is deprecated! Use the VagrantCloud.gitlab-ci.yml template instead"
  # Exit on any error
  set -e
  # create new box (don't check the status because it will return 422 if the box exists)
  curl \
    --header "Content-Type: application/json" \
    --header "Authorization: Bearer $VAGRANTCLOUD_TOKEN" \
    ${VAGRANTCLOUD_API_URL}/boxes \
    --data "{ \"box\": { \"username\": \"esss\", \"name\": \"${box_name}\", \"is_private\": false } }"
  # create new version
  curl --fail \
    --header "Content-Type: application/json" \
    --header "Authorization: Bearer $VAGRANTCLOUD_TOKEN" \
    ${box_api_url}/versions \
    --data "{ \"version\": { \"version\": \"${box_version}\" } }"
  # create new provider
  curl --fail \
    --header "Content-Type: application/json" \
    --header "Authorization: Bearer $VAGRANTCLOUD_TOKEN" \
    ${box_api_url}/version/${box_version}/providers \
    --data '{ "provider": { "name": "virtualbox" } }'
  # get the path to upload the box
  upload_path=$(curl --header "Authorization: Bearer $VAGRANTCLOUD_TOKEN" \
    ${box_api_url}/version/${box_version}/provider/virtualbox/upload | jq -r .upload_path)
  # upload the box
  curl --fail $upload_path --request PUT --upload-file ${box_path}
  # release the new version
  curl --fail \
    --header "Authorization: Bearer $VAGRANTCLOUD_TOKEN" \
    --request PUT \
    ${box_api_url}/version/${box_version}/release
}
