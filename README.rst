ESS ICS GitLab CI shared functions
==================================

**This repository is deprecated.**
Use specific docker images or the `gitlab-ci.yml templates <https://gitlab.esss.lu.se/ics-infrastructure/gitlab-ci-yml>`_.

This repository includes functions that can be re-used in your GitLab CI job.

Add the following in your `.gitlab-ci.yml`::

    before_script:
      - curl -s -o /tmp/functions.sh https://gitlab.esss.lu.se/ics-infrastructure/ics-shared-gitlab-ci-functions/raw/master/functions.sh
      - . /tmp/functions.sh

Inspired by https://stackoverflow.com/questions/47790403/share-gitlab-ci-yml-between-projects
